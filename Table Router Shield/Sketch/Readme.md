# Table Router Shield Sketch
This sketch is intended to work with the table router shield on an ESP32

## Changelog
### v1.0
* Initial release

## Todo
This sketch is still under development. Open issues / features:
* Add more detailed documentation and configuration
* Functionality to move from endstop when in direct mode
* Abort driving from endstop after a while and show an error message in the display
* Use display for help texts
* Fix current position and target position for direct mode
* Implement functionalities for controlling the spinde. This includes 0-10V Analog output und the start/stop button